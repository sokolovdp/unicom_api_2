from django.apps import AppConfig


class UApiConfig(AppConfig):
    name = 'u_api'
