from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.core.validators import RegexValidator
from django.db import models

# Create your models here.

class ApiUserManager(BaseUserManager):
    """ Helps Django to work with our UserModel """

    def create_user(self, email=None, name=None, password=None, group="partner"):
        if not email:
            raise ValueError('Users must have an email address')
        email = self.normalize_email(email)
        user = self.model(email=email, name=name)
        user.group = group
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email=None, name=None, password=None, group="superuser"):
        superuser = self.create_user(email=email, name=name, password=password)
        superuser.is_superuser = True
        superuser.is_staff = True
        superuser.group = group
        superuser.save(using=self._db)
        return superuser


class ApiUserAccount(AbstractBaseUser, PermissionsMixin):
    USER_GROUP = (
        ('finance', 'bank or agency'),
        ('partner', 'retail partners'),
        ('superuser', 'API & DB administrator'),
    )

    name = models.CharField(max_length=80, unique=True)
    email = models.EmailField(max_length=80, unique=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    group = models.CharField(max_length=10, unique=False, choices=USER_GROUP, default="partner")
    created = models.DateField(auto_now_add=True)

    USERNAME_FIELD = 'name'
    REQUIRED_FIELDS = ['email', ]

    objects = ApiUserManager()

    def get_full_name(self):
        """ Django uses it to get a user full name """
        return self.name

    def get_short_name(self):
        """Django uses this when it needs to get the users abbreviated name."""
        return self.name

    def __str__(self):
        """Django uses this when it needs to convert the object to text."""
        return self.name


class Offer(models.Model):
    OFFER_TYPE = (
        ('потреб', 'потребительский кредит'),
        ('ипотека', 'кредит на жилье'),
        ('авто', 'кредит на автомобиль'),
        ('КМСБ', 'кредит малому и среднему бизнесу'),
    )

    created = models.DateField(auto_now_add=True)
    modified = models.DateField(blank=True, null=True)
    rotation_started = models.DateField(blank=True, null=True)
    rotation_ended = models.DateField(blank=True, null=True)
    title = models.CharField(max_length=100, unique=True)
    type = models.CharField(max_length=10, blank=False, choices=OFFER_TYPE)
    minimal_score = models.PositiveIntegerField(blank=False)
    maximal_score = models.PositiveIntegerField(blank=False)
    finance = models.ForeignKey(ApiUserAccount, on_delete=models.CASCADE, blank=False)

    def __str__(self):
        return self.title


class Client(models.Model):
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number format: '+999999999'. Up to 15 digits.")
    created = models.DateField(auto_now_add=True)
    modified = models.DateField(blank=True, null=True)
    first_name = models.CharField(max_length=80, blank=False)
    second_name = models.CharField(max_length=80, blank=False)
    family_name = models.CharField(max_length=80, blank=False)
    birthday = models.DateField(blank=False)
    phone_number = models.CharField(validators=[phone_regex], max_length=15, blank=False, unique=True)
    passport_number = models.CharField(max_length=12, blank=False, unique=True)
    credit_score = models.PositiveIntegerField(blank=False)

    def __str__(self):
        return self.family_name + ' ' + self.first_name


class Application(models.Model):
    APPLICATION_STATUS = (
        ('NEW', 'created'),
        ('SENT', 'published'),
        ('RECEIVED', 'finance received application')
    )
    created = models.DateField(auto_now_add=True)
    published = models.DateField(blank=True, null=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    offer = models.ForeignKey(Offer,   on_delete=models.CASCADE)
    finance = models.ForeignKey(ApiUserAccount, on_delete=models.CASCADE)
    status = models.CharField(max_length=10, choices=APPLICATION_STATUS, default="NEW")

    def __str__(self):
        return self.client.first_name + ' : ' + self.finance.name + ' - ' + self.offer.title
