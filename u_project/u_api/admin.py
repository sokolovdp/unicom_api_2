from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth.models import Group

from . import models


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = models.ApiUserAccount
        fields = ('name', 'email', 'group')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = models.ApiUserAccount
        fields = ('name', 'email', 'group', 'password', 'is_active')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class ApiUserAdmin(BaseUserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm

    list_display = ['name', 'email', 'group', 'created']  # add fields to the list view
    list_filter = ['group']  # add filter column
    fieldsets = ((None, {'fields': ('email', 'password', 'group')}),
                 ('Permissions', {'fields': ('is_superuser',)}),)
    add_fieldsets = ((None, {
        'classes': ('wide',),
        'fields': ('name', 'email', 'group', 'password1', 'password2')}
                      ),)

    search_fields = ['name', 'email', 'group']  # create search box
    ordering = ('group',)
    filter_horizontal = ()
    list_editable = ['email', 'group']  # make fields editable from the view


admin.site.unregister(Group)
admin.site.register(models.ApiUserAccount, ApiUserAdmin)


class OfferAdmin(admin.ModelAdmin):
    fields = ['title', 'finance', 'type', 'minimal_score', 'maximal_score',  # 'created',
              'modified', 'rotation_started', 'rotation_ended']  # in this order fields will appear
    search_fields = ['title', 'type', 'finance']  # create search box
    list_filter = ['type', 'finance']  # add filter column
    list_display = ['title', 'finance', 'type', 'minimal_score', 'maximal_score', 'created',
                    'modified', 'rotation_started', 'rotation_ended']  # add fields to the list view
    list_editable = ['finance', 'type', 'minimal_score', 'maximal_score']  # make fields editable from the view


class ClientAdmin(admin.ModelAdmin):
    fields = ['family_name', 'first_name', 'second_name', 'credit_score', 'birthday', 'passport_number',
              'phone_number', 'modified', ]
    search_fields = ['family_name', 'first_name', 'passport_number', 'phone_number']  # create search box
    list_filter = ['credit_score', ]  # add filter column
    list_display = ['created', 'family_name', 'first_name', 'second_name', 'credit_score', 'birthday',
                    'passport_number',
                    'phone_number', 'modified', ]  # add fields to the list view
    list_editable = ['family_name', 'first_name', 'second_name', 'credit_score', 'phone_number',
                     'passport_number', ]  # make fields editable from the view


class ApplicationAdmin(admin.ModelAdmin):
    fields = ['client', 'finance', 'offer', 'status', ]  # in this order fields will appear
    search_fields = ['client', 'finance', 'offer', 'status']  # create search box
    list_filter = ['client', 'finance', 'offer', 'status']  # add filter column
    list_display = ['created', 'client', 'finance', 'offer', 'status', 'published']  # add fields to the list view
    list_editable = ['client', 'finance', 'offer', 'status', 'published']  # make fields editable from the view


admin.site.register(models.Offer, OfferAdmin)
admin.site.register(models.Client, ClientAdmin)
admin.site.register(models.Application, ApplicationAdmin)
