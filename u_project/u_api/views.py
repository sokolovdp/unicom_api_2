from rest_framework import viewsets, filters, mixins
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from . import models, permissions, serializers


class HelloApiView(APIView):
    def get(self, request, format=None):
        an_api_view = [
            'UNICOM Services',
            'Partner APIs: api/partner/ - to work with client data + api/application/ - to send applications to banks',
            'Finance API: api/finance/',
            'you must be authorized to use services'
        ]
        return Response({'message': 'Hello!', 'an_api_view': an_api_view})


class LoginViewSet(viewsets.ViewSet):
    serializer_class = AuthTokenSerializer

    def create(self, request, *args, **kwargs):  # uses ObtainAuthToken APIview
        return ObtainAuthToken().post(request)


class PartnerViewSet(mixins.CreateModelMixin,
                     mixins.RetrieveModelMixin,
                     mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, permissions.PartnerApiPermission,)
    serializer_class = serializers.ClientSerializer
    queryset = models.Client.objects.all().order_by("id")
    filter_backends = (filters.SearchFilter,)
    search_fields = ('first_name', 'second_name', 'family_name', "phone_number", "passport_number")


class CreateApplicationViewSet(mixins.CreateModelMixin,
                               viewsets.GenericViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, permissions.PartnerApiPermission,)
    serializer_class = serializers.ApplicationSerializer
    queryset = models.Application.objects.all().order_by("id")


class FinanceViewSet(mixins.RetrieveModelMixin,
                     mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, permissions.FinanceApiPermission,)
    queryset = models.Application.objects.all().order_by("id")
    serializer_class = serializers.ApplicationSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('status', 'offer__title', 'finance__name',)

    # def list(self, request, *args, **kwargs):
    #     offer_title = self.request.query_params.get('offer', None)
    #     all_applications = self.queryset.filter(finance=request.user.id)   # .filter(status="SENT")
    #     if offer_title is not None:
    #         try:
    #             offer = models.Offer.objects.get(title=offer_title)
    #         except models.Offer.DoesNotExist:
    #             all_applications = []
    #         else:
    #             all_applications = all_applications.filter(offer=offer.id)
    #     all_applications_json = serializers.ApplicationSerializer(all_applications, many=True)
    #     return Response(all_applications_json.data)

    def retrieve(self, request, pk=None, *args, **kwargs):
        application = self.get_object()
        if application.finance.id == request.user.id:
            if application.status == 'SENT':
                application.status = 'RECEIVED'
                application.save()
            application_json = serializers.ApplicationSerializer(application)
            return Response(application_json.data)
        else:
            return Response(
                {"error": "your id={}, so you can't modify application of another organization with id={}".format(
                    request.user.id, application.finance.id)}, status=401)

#
# class AdminViewSet(viewsets.ModelViewSet):
#     authentication_classes = (TokenAuthentication,)
#     permission_classes = (IsAuthenticated, permissions.AdminApiPermission,)
