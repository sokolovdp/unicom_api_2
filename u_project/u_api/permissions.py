from rest_framework import permissions


class FinanceApiPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.group == "superuser":
            return True
        return request.user.group == "finance"


class PartnerApiPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.group == "superuser":
            return True
        return request.user.group == "partner"


class AdminApiPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.group == "superuser"