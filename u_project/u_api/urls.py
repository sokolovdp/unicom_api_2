from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from . import views

app_name = 'u_api'

router = DefaultRouter()
router.register('partner', views.PartnerViewSet, base_name='partner')
router.register('application', views.CreateApplicationViewSet, base_name='application')
router.register('finance', views.FinanceViewSet, base_name='finance')
router.register('login', views.LoginViewSet, base_name='login')
# router.register('admin', views.AdminViewSet, base_name='admin')

urlpatterns = [
    url(r'^hello', views.HelloApiView.as_view(), name='hello'),
    url(r'', include(router.urls)),
]