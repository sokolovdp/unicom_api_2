## UNICOM Django REST API for partners & banks

1) api/partner/ - partner's API to work with client data
allowed methods: create, retrive, list.
**Search fields** are: 'first_name', 'second_name', 'family_name', "phone_number", "passport_number"

Client profile create request format:
```
header key: Content-Type value: application/json
POST body: { "first_name": "User1", "second_name": "Father1", 
"family_name": "Family1", birthday": "2000-01-01", 
"phone_number": "+87252555552",   "passport_number": "87252555552", 
"credit_score": "55" }
```
2) api/application/ - partner API to create client applications

Application create request format:
```
header key: Content-Type value: application/json
POST body: { "client":  1,  "offer": 1, "finance": 2 }
```

3) finance/ - bank's API to client's applications, allowed methods: 
retrive, list.
**Search fields**  are: 'status', 'offer__title', 'finance__name'


## Authorization is based on JWT
authorization point: **api/login/**

Login request format:
```
header key: Content-Type  value: application/json
POST body: { "username": "partner1", "password": "jwhewjhekq"}
```
in all following requests must be present header with token:
```
header key: Authorization  value: 'Token 182761876238176238176'
```